<?php

require "vendor/autoload.php";
use Michelf\Markdown;

$loader = new \Twig\Loader\FilesystemLoader(dirname(__FILE__) . '/views');

$twigConfig = array(
    // 'cache' => './cache/twig/',
    // 'cache' => false,
    'debug' => true,
);

Flight::register('view', '\Twig\Environment', array($loader, $twigConfig), function ($twig) {
    $twig->addExtension(new \Twig\Extension\DebugExtension()); // Add the debug extension
    
    $twig->addFilter(new \Twig\TwigFilter('markdown', function($string){
        return Markdown::defaultTransform($string);
    }));
});

Flight::map('render', function($template, $data=array()){
    Flight::view()->display($template, $data);
});

Flight::route('/',function(){

    Flight::render('index.twig');
});

Flight::route('/zerg/@slug', function($slug){
    $data = array(
        'unit' => get_zerg_by_slug($slug),
        'race' => 'zerg',
    );

    Flight::render('one_unit.twig', $data);
});

Flight::route('/terran/@slug', function($slug){
    $data = array(
        'unit' => get_terran_by_slug($slug),
        'race' => 'terran',
    );

    Flight::render('one_unit.twig', $data);
});

Flight::route('/terran', function(){
    $data = array(
        'listing' => get_terran(),
        'race' => 'terran',
    );
    
    Flight::render('listing.twig', $data);
});

Flight::route('/zerg', function(){
    $data = array(
        'listing' => get_zerg(),
        'race' => 'zerg',
    );
    
    Flight::render('listing.twig', $data);
});



Flight::start();
